qonk (0.3.1-3.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Rebuild for Debian Buster.
  * debian: Apply "wrap-and-sort -abst".
  * debian/control:
    + Bump debhelper compat to v12.
    + Bump Standards-Version to 4.3.0.
    + Use Rules-Requires-Root: no.
    + Update Vcs-* fields to point to git repo under Salsa games-team.
    + Remove build-dependency on old automake1.11.
      (Closes: #865199)
    + Replace legacy dependency on ttf-dejavu with fonts-dejavu.
  * debian/rules:
    + Switch to debhelper and dh syntax.
    + Enable full hardening.
    + Use qonk.links file to create symlinks.
  * debian/README.source: Removed, no longer needed.
  * debian/menu: Removed per ctte's decision on desktop/menu files.

 -- Boyuan Yang <byang@debian.org>  Fri, 18 Jan 2019 10:21:54 -0500

qonk (0.3.1-3.1) unstable; urgency=low

  * Non-maintainer upload.
    * Team intern so not strictly following the procedures
  * Fix build failure (Closes: #541443)
    * Set DEB_AUTO_UPDATE_AUTOMAKE to 1.11 and specify automake dependency
      to 1.11 so build success does not depend on any specific version being
      `automake'
    * Add DEB_AUTO_UPDATE_ACLOCAL to build with automake 1.11
  * Bump standards Version to 3.8.3
    * Add README.source copied and adapted from supertuxkart

 -- Christoph Egger <debian@christoph-egger.org>  Wed, 30 Sep 2009 12:30:37 +0200

qonk (0.3.1-3) unstable; urgency=low

  [ Barry deFreese ]
  * 50_absolute_mouse.diff - Use absolute mouse positioning. (Closes: #480170).
    + Thanks to Christian Pulvermacher for the patch!.

 -- Barry deFreese <bdefreese@debian.org>  Fri, 10 Apr 2009 13:03:25 -0400

qonk (0.3.1-2) unstable; urgency=low

  [ Eddy Petrișor ]
  * fix Vcs-Svn URL to point to svn, not the incomplete ssh location

  [ Martín Ferrari ]
  * Updated email address.

  [ Barry deFreese ]
  * 30_gcc_4.4.diff - Build with gcc-4.4. (Closes: #504898).
    + Thanks to Martin Michlmayr for the fix.
  * 40_player_colors.diff - Fix message colors. (Closes: #447215).
    + Thanks to Christian Pulvermacher for the hint!
  * Update my e-mail address.
  * Fix up some debian/copyright syntax.
  * Bump Standards Version to 3.8.1.

 -- Barry deFreese <bdefreese@debian.org>  Tue, 07 Apr 2009 22:05:58 -0400

qonk (0.3.1-1) unstable; urgency=low

  [ Barry deFreese ]
  * New upstream release.
  * Move from simple-patch-sys to quilt.
  * Remove 00-invalid_operator.patch, no longer necessary.
  * 10_build_environment.diff - Fix upstream build issues.
    + Thanks to Ben Asselstine for the fix.
  * Autoreconf on build to pick up fixes from patch.
  * Add build-deps for autoconf, automake, and libtool.
  * Add myself to uploaders.
  * Add VCS fields in control.
  * Add desktop file.
  * 20_gcc_4.3.diff - Build with gcc-4.3. (Closes: #462052).
  * Use ttf-dejavu instead of bitstream-vera. (Closes: #455583, #461288).
  * Bump Standards Version to 3.7.3. (No changes needed).

 -- Barry deFreese <bddebian@comcast.net>  Sun, 18 Nov 2007 22:59:58 -0500

qonk (0.3.0-2) unstable; urgency=low

  * A typo in upstream code prevented from locating the font.ttf file.
    (Closes: #446715)

 -- Martín Ferrari <martin.ferrari@gmail.com>  Tue, 16 Oct 2007 02:50:22 -0300

qonk (0.3.0-1) unstable; urgency=low

  * Initial release (Closes: #373966) after a year of waiting for upstream to
    resolve licensing issues.
  * Ignored provided font.ttf, as it is a copy of VeraMono.ttf from
    ttf-bitstream-vera. Instead, I put a symlink and a dependency.
  * Added license notice for the font, although we don't ship it in the
    binary.

 -- Martín Ferrari <martin.ferrari@gmail.com>  Mon, 08 Oct 2007 17:41:01 -0300
